package com.bbbug.coree.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class Lrcinfo implements Serializable {
    private static final long serialVersionUID = 2965501777518355613L;
    //歌曲mid
    private Integer id;
    //歌曲歌词
    private String lrc;
    //歌曲被点了多少次
    private Integer count;
}
