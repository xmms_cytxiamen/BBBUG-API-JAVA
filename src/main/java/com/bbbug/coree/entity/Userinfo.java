package com.bbbug.coree.entity;

import com.fasterxml.jackson.core.JsonFactory;
import lombok.Data;

import java.io.Serializable;
@Data
public class Userinfo extends JsonFactory implements Serializable {
    private static final long serialVersionUID = -80527605734658904L;
    /**
     * 用户ID
     */
    private Integer  user_id;
    /**
     * 1男0女
     */
    private Integer  user_sex;
    /**
     * 邮箱
     */
    private String  user_account;
    /**
     * 戳一戳
     */
    private String  user_touchtip;
    /**
     * 昵称
     */
    private String  user_name;
    /**
     * 头像URL
     */
    private String  user_head;
    /**
     * 用户签名
     */
    private String  user_remark;
    /**
     * 点歌数量
     */
    private Integer  user_song;
    /**
     * 发图数量
     */
    private Integer  user_img;
    /**
     * 猜歌得分
     */
    private Integer  user_gamesongscore;
    /**
     * 是否为移动端
     */
    private Integer  user_app;
    /**
     * 登陆设备
     */
    private String  user_device;
    /**
     * apc看点
     */
    private String  appname;
    /**
     * 密码
     */
    private String  user_passwd;
    /**
     * vip
     */
    private String user_vip;
}
