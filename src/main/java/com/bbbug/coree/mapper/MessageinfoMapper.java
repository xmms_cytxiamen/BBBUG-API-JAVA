package com.bbbug.coree.mapper;

import com.bbbug.coree.entity.Messageinfo;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface MessageinfoMapper {
    /**
     * 通过ID查询单条数据
     *
     * @param messageId 主键
     * @return 实例对象
     */
    @Select("select * from messageinfo where message_id=#{messageId} ")
    Messageinfo queryById(String messageId);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Select("select message_id, message_user, message_type, message_where, message_to, message_content, message_status, message_createtime, message_updatetime from messageinfo limit #{offset}, #{limit}")
    List<Messageinfo> queryAllByLimit(int offset, int limit);
    @Select("SELECT * FROM `messageinfo` where message_to=#{room_id} ORDER BY message_createtime DESC")
    List<Messageinfo> queryForList(String room_id);


    /**
     * 新增数据
     *
     * @param messageinfo 实例对象
     * @return 实例对象
     */
    @Insert("insert into messageinfo (message_id,message_user, message_type, message_where, message_to, message_content, message_status, message_createtime, message_updatetime) value (#{message_id},#{message_user},#{message_type},#{message_where},#{message_to},#{message_content},#{message_status},#{message_createtime},#{message_updatetime})")

     Boolean insert(Messageinfo messageinfo);

    /**
     * 修改数据
     *
     * @param messageinfo 实例对象
     * @return 实例对象
     */
    @Update("<script> " +
            "UPDATE messageinfo SET " +
            "<if test='message_user != null '> message_user=#{message_user},</if>" +
            "<if test='message_type != null '> message_type=#{message_type},</if>" +
            "<if test='message_where != null and message_where!=\"\" '> message_where=#{message_where},</if>"+
            "<if test='message_to != null '> message_to=#{message_to},</if>"+
            "<if test='message_content != null and message_content!=\"\" '> message_content=#{message_content},</if>"+
            "<if test='message_status != null '> message_status=#{message_status},</if>"+
            "<if test='message_createtime != null '> message_createtime=#{message_createtime},</if>"+
            "<if test='message_updatetime != null '> message_updatetime=#{message_updatetime},</if>"+
            "where message_id = #{message_id} " +
            "</script>")
    Messageinfo update(Messageinfo messageinfo);

    /**
     * 通过主键删除数据
     *
     * @param messageId 主键
     * @return 是否成功
     */
    @Delete("delete from messageinfo where message_id = #{messageId}")
    boolean deleteById(String messageId);

    @Delete("delete from messageinfo where message_to = #{room_id}")
    boolean deleteByroomid(Integer room_id);

}
